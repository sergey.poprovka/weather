<?php
    require 'classes/Weather.php';

    $weather = new Weather(isset($_GET['location']) ? $_GET['location'] : "Poltava,UA");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Weather</title>
</head>
<body>
 <div class="container">
     <div class="row">
         <div class="col-6 offset-3">
             <div class="card card-body shadow-lg mt-5 text-center">
                 <form method="get" action="index.php">
                     <div class="form-group">
                         <label for="location">Location</label>
                         <input class="form-control" type="text" name="location" id="location" />
                     </div>
                     <div class="form-group">
                         <button type="submit" class="btn btn-outline-success btn-lg">Get weather</button>
                     </div>
                 </form>
             </div>
             <div class="card card-body shadow-lg mt-5 text-center">
                 <div>
                     <img src="<?=$weather->getIcon()?>" />
                 </div>
                 <h2><?=$weather->getLocation()?></h2>
                 <p><?=$weather->getDescription()?></p>
                 <hr class="mt-3 mb-3">
                 <ul class="list-unstyled">
                     <li><strong>Temperature: </strong><?=$weather->getTemperature()?> C<sup>o</sup></li>
                     <li><strong>Humidity: </strong><?=$weather->getHumidity()?> </li>
                     <li><strong>Pressure: </strong><?=$weather->getPressure()?> </li>
                     <li><strong>Sunrise: </strong><?=$weather->getSunrise()?> </li>
                     <li><strong>Sunset: </strong><?=$weather->getSunset()?> </li>
                     <li><strong>Temp min: </strong><?=$weather->getTempMin()?> </li>
                     <li><strong>Temp max: </strong><?=$weather->getTempMax()?> </li>
                     <li><strong>Wind speed: </strong><?=$weather->getWindSpeed()?> </li>
                 </ul>
             </div>
         </div>
     </div>
 </div>
</body>
</html>
