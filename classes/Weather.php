<?php
    class Weather{

        private $location;
        private $units;
        private $protocol = "https";
        private $base_url = "openweathermap.org";
        private $api_url;
        private $weather_url;
        private $api_key = "e3f0ee26575fe782096cc9e510c0abb4";

        private $temperature;
        private $humidity;
        private $pressure;
        private $temp_max;
        private $temp_min;
        private $general;
        private $description;
        private $wind_speed;
        private $icon;
        private $latitude;
        private $longitude;
        private $sunrise;
        private $sunset;

        /**
         * @return mixed
         */
        public function getLocation()
        {
            return $this->location;
        }

        /**
         * @return mixed
         */
        public function getTemperature()
        {
            return $this->temperature;
        }

        /**
         * @return mixed
         */
        public function getHumidity()
        {
            return $this->humidity;
        }

        /**
         * @return mixed
         */
        public function getPressure()
        {
            return $this->pressure;
        }

        /**
         * @return mixed
         */
        public function getTempMax()
        {
            return $this->temp_max;
        }

        /**
         * @return mixed
         */
        public function getTempMin()
        {
            return $this->temp_min;
        }

        /**
         * @return mixed
         */
        public function getGeneral()
        {
            return $this->general;
        }

        /**
         * @return mixed
         */
        public function getDescription()
        {
            return $this->description;
        }

        /**
         * @return mixed
         */
        public function getWindSpeed()
        {
            return $this->wind_speed;
        }

        /**
         * @return mixed
         */
        public function getIcon()
        {
            return $this->icon;
        }

        /**
         * @return mixed
         */
        public function getLatitude()
        {
            return $this->latitude;
        }

        /**
         * @return mixed
         */
        public function getLongitude()
        {
            return $this->longitude;
        }

        /**
         * @return mixed
         */
        public function getSunrise()
        {
            return $this->sunrise;
        }

        /**
         * @return mixed
         */
        public function getSunset()
        {
            return $this->sunset;
        }

        public function __construct($location, $units = "metric"){
            $this->location = $location;
            $this->units = $units;
            $this->construct_urls();
            $this->fill_data();
        }

        private function construct_urls(){
            $this->construct_api_url();
            $this->construct_weather_url();
        }

        private function construct_api_url(){
            $this->api_url = $this->protocol."://" . "api." . $this->base_url;
        }

        private function construct_weather_url(){
            $this->weather_url =  $this->api_url . "/data/2.5/weather?q=" . $this->location . "&APPID=" . $this->api_key . "&units=" . $this->units;
        }

        private function get_data(){
            $curl = curl_init($this->weather_url);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($curl);

            return $this->decode_data($result);
        }

        private function decode_data($data){
            return json_decode($data);
        }

        private function fill_data(){
            $data = $this->get_data();

            $this->temperature = $data->main->temp;
            $this->humidity = $data->main->humidity;
            $this->pressure = $data->main->pressure;
            $this->temp_max = $data->main->temp_max;
            $this->temp_min = $data->main->temp_min;
            $this->general = $data->weather[0]->main;
            $this->description = $data->weather[0]->description;
            $this->wind_speed = $data->wind->speed;
            $this->icon = $this->get_icon($data->weather[0]->icon);
            $this->latitude = $data->coord->lat;
            $this->longitude = $data->coord->lon;
            $this->sunrise = $this->parse_time($data->sys->sunrise);
            $this->sunset = $this->parse_time($data->sys->sunset);
        }

        private function get_icon($code){
            return $this->protocol . "://" . $this->base_url ."/img/wn/".$code."@2x.png";
        }

        private function parse_time($unixtime){
            return date("H:i:s", (preg_match('/^[0-9]*$/', $unixtime)) ? $unixtime : strtotime($unixtime));
        }
    }
?>